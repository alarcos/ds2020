var self;

function ViewModel() {
	
	self = this;
	


	self.login = function() {
		var info = {
				type : "Login",
				userName : loginUserName.value,
				pwd : loginPwd.value
			};
			
			var data = {
				data : JSON.stringify(info),
				url : "login",
				type : "post",
				contentType: 'application/json',
				success : function() {
					window.location.href="games.html";
				},
				error : function(response) {
					alert(response.message);
				}
			};
			$.ajax(data);
		
	}
	
	self.register = function() {
		var info = {
			type : "Register",
			userName : registroUserName.value,
			email : registroEmail.value,
			pwd1 : pwd1.value,
			pwd2 : pwd2.value
		};
		var data = {
				data : JSON.stringify(info),
				url : "register",
				type : "post",
				contentType: 'application/json',

				success : function() {
					alert("OK");
				},
				error : function(response) {
					alert(response.responseText);
				}
			};
			$.ajax(data);
	}

	self.requestToken = function(){
		alert("En requestToken");
	}

	self.logout = function(){
    	var data = {
    			url : "logout",
    			type : "post",
    			
    			success : function() {
    				window.location.href= "index.html";
    			},
    			error : function(response) {
    				alert(response.message);
    			}
    		};
    		$.ajax(data);
    }
}

$(document).ready(function() {
	$("#mostrarLogin").click(function() {
		$("#divLogin").show();
		$("#divRegistro").hide()
	});
	$("#mostrarRegistro").click(function() {
		$("#divLogin").hide();
		$("#divRegistro").show()
	})
});


var vm = new ViewModel();
ko.applyBindings(vm);

