function ViewModel() {
	var self = this;
	
	
	self.logout = function(){
    	var data = {
    			url : "logout",
    			type : "post",
    			
    			success : function() {
    				window.location.href= "index.html";

    			},
    			error : function(response) {
    				alert(response.message);
    			}
    		};
    		$.ajax(data);
    }
	
	self.joinToMatch = function() {
		var info = {
			type : "JoinToMatch",
			game : selectGames.value
		};
		
		var data = {
			data : JSON.stringify(info),
			url : "joinToMatch",
			type : "post",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				
				if (response.type=="match") {
					var match = response.match;
					sessionStorage.idMatch = match.idMatch;
					sessionStorage.started = match.started;
					if (info.game == "Tres en raya")
						window.location.href = "ter.html";
					else 
						window.location.href= info.game+".html";
				}
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);
	}

	
	function getGames () {
		var data = {

				url : "getGames",
				type : "get",
				
				success : function(response) {
					var options ="";
					for (var i=0; i<response.length; i++) {
						options = options + "<option>" + response[i] + "</option>";
					}
					selectGames.innerHTML = options;
				},
				error : function(response) {
					alert(response.message);
				}
			};
			$.ajax(data);
	}
	getGames();

}

var vm = new ViewModel();
ko.applyBindings(vm);