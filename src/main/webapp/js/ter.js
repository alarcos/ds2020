var self;

function ViewModel() {
	self = this;

	self.line1 = ko.observableArray([]);
	self.line2 = ko.observableArray([]);
	self.line3 = ko.observableArray([]);
	self.mensaje = ko.observable("");
	self.player1 = ko.observable("");
	self.player2 = ko.observable("");
		
	var idMatch = sessionStorage.idMatch;
	var started = JSON.parse(sessionStorage.started);
	if (started) {
		self.mensaje("La partida " + idMatch + " ha comenzado");
	} else {
		self.mensaje("Esperando oponente para la partida " + idMatch);
	}
	
	var url = "ws://" + window.location.host+ "/juegos";
	self.ws = new WebSocket(url);

	self.ws.onopen = function(event) {
		var msg = {
			type : "ready",
			idMatch : sessionStorage.idMatch
		};
		self.ws.send(JSON.stringify(msg));
	}

	self.ws.onmessage = function(event) {

				var data = event.data;
				var i;
				var table;
				
				data = JSON.parse(data);
				switch(data.type){
				case "matchStarted":
					self.mensaje("La partida ha empezado");
					
					var players = data.players;
					self.player1(players[0].userName);
					self.player2(players[1].userName);
					
					
					table = data.startData.table;
					for (i=0; i<3; i++) {
						self.line1.push(table[i]);
					}
					for (i=3; i<6; i++) {
						self.line2.push(table[i]);
					}
					for (i=6; i<9; i++) {
						self.line3.push(table[i]);
					}
				break;
				case "winner":
					alert("Ha ganado "+ data.winner);
				case "tablero":
					self.mensaje("Es el turno de "+data.turn);
					
					self.line1.splice(0,9);
					self.line2.splice(0,9);
					self.line3.splice(0,9);
					
					table = data.table;
					for (i=0; i<3; i++) {
						self.line1.push(table[i]);
					}
					for (i=3; i<6; i++) {
						self.line2.push(table[i]);
					}
					for (i=6; i<9; i++) {
						self.line3.push(table[i]);
					}
				break;
				case "error":
					alert(data.msg);
					break;
				}		
		
			
	}
	self.ponerEnMesa = function(position) {
		var msg = {
			type : "movimiento",
			idMatch : sessionStorage.idMatch,
			position : position
		};
		try{
			self.ws.send(JSON.stringify(msg));
		}catch(error){
			alert(error);
		}
	}
	self.logout = function(){
    	var data = {
    			url : "logout",
    			type : "post",
    			
    			success : function() {
    				window.location.href= "index.html";

    			},
    			error : function(response) {
    				alert(response.message);
    			}
    		};
    		$.ajax(data);
    }
}



var vm = new ViewModel();
ko.applyBindings(vm);