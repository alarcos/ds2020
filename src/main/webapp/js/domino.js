var self;

function ViewModel() {
	self = this;
	self.usuarios = ko.observableArray([]);
	self.table = ko.observableArray([]);
	self.fichas = ko.observableArray([]);
	self.init = false;
	
	self.mensaje = ko.observable("");
		
	var idMatch = sessionStorage.idMatch;
	var started = JSON.parse(sessionStorage.started);
	if (started) {
		self.mensaje("La partida " + idMatch + " ha comenzado");
	} else {
		self.mensaje("Esperando oponente para la partida " + idMatch);
	}
	
	var url = "ws://" + window.location.host+ "/juegos";
	self.ws = new WebSocket(url);

	self.ws.onopen = function(event) {
		var msg = {
			type : "ready",
			idMatch : sessionStorage.idMatch
		};
		self.ws.send(JSON.stringify(msg));
	}

	self.ws.onmessage = function(event) {

				var data = event.data;
				var table;
				var i;
				
				data = JSON.parse(data);
				switch(data.type){
				case "matchStarted":
					self.mensaje("La partida ha empezado\n" +
							"Es el turno de "+data.startData.turn);
					self.init = true;
					var players = data.players;
					for (i=0; i<players.length; i++) {
						var player = players[i];
						self.usuarios.push(player.userName);
					}
					table = data.startData.table;
					for (i=0; i<table.length; i++) {
						self.table.push(table[i]);
					}
					table = data.startData.data;
					for (i=0; i<table.length; i++) {
						self.fichas.push(table[i]);
					}
					
					
				break;
				case "winner":
					alert("Ha ganado "+ data.winner);
				case "tablero":
					self.mensaje("Es el turno de "+data.turn);
					
					self.table.removeAll();
					self.fichas.removeAll();

					
					table = data.table;
					for (i=0; i<table.length; i++) {
						self.table.push(table[i]);
					}
					table = data.data;
					for (i=0; i<table.length; i++) {
						self.fichas.push(table[i]);
					}
				break;
				case "error":
					self.mensaje("Es el turno de "+data.turn);
					alert(data.msg);
					break;
				}		
		
			
	}
	self.ponerEnMesa = function(tile) {
		var msg = {
			type : "movimiento",
			idMatch : sessionStorage.idMatch,
			robar : false,
			tile : tile
		};
		try{
			self.ws.send(JSON.stringify(msg));
		}catch(error){
			alert(error);
		}
	}
	self.robar = function() {
			if(self.init){
				var msg = {
					type : "movimiento",
					idMatch : sessionStorage.idMatch,
					robar : true
				};
				try{
					self.ws.send(JSON.stringify(msg));
				}catch(error){
					alert(error);
				}
			}
	}
	self.logout = function(){
    	var data = {
    			url : "logout",
    			type : "post",
    			
    			success : function() {
    				window.location.href= "index.html";

    			},
    			error : function(response) {
    				alert(response.message);
    			}
    		};
    		$.ajax(data);
    }
}



var vm = new ViewModel();
ko.applyBindings(vm);