package edu.uclm.esi.games2020.http;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.esi.games2020.exceptions.MyException;
import edu.uclm.esi.games2020.model.Manager;
import edu.uclm.esi.games2020.model.Match;
import edu.uclm.esi.games2020.model.User;

@RestController
public class Controller {
	private static String error = "error";
	private static String message = "message";
	private String matchMsg = "match";
	
	
	@PostMapping("/login")
	public void login(HttpSession session, @RequestBody Map<String, Object> credenciales) throws MyException {
		JSONObject jso = new JSONObject(credenciales);
		String userName = jso.getString("userName");
		String pwd = jso.getString("pwd");
		User user = Manager.get().login(session, userName, pwd);
		session.setAttribute("user", user);
	}
	@PostMapping("/logout")
	public void logout(HttpSession session) throws MyException {

			User user=(User) session.getAttribute("user");
			if (user!=null) {
				Manager.get().logout(user);
				session.invalidate();
			}
			else throw new MyException("Usuario desconocido");

	}
	@PostMapping("/register")
	public String register(HttpSession session, @RequestBody Map<String, Object> credenciales){
		
		
		
		JSONObject jso = new JSONObject(credenciales);
		String userName = jso.getString("userName");
		String email = jso.getString("email");
		String pwd1 = jso.getString("pwd1");
		String pwd2 = jso.getString("pwd2");
		
		JSONObject resultado = new JSONObject();
		
		if(pwd1.equals(pwd2)) {
				try {
					Manager.get().register(email, userName, pwd1);
					resultado.put("type", "OK");
				}
				catch (Exception e) {
					resultado.put("type", error);
					resultado.put(message, e.getMessage());
				}
		}else {
			resultado.put("type", error);
			resultado.put(message, "las password no coinciden.");
		}
		
		
		return resultado.toString();
	}
	@PostMapping("/resetpwd")
	public String resetpwd(HttpSession session, @RequestBody Map<String, Object> credenciales) {
		JSONObject jso = new JSONObject(credenciales);
		JSONObject resultado = new JSONObject();
		
		try {
			User user=(User) session.getAttribute("user");
			
			if (!jso.getString("type").equals("ResetPwd")) {
				resultado.put("type", error);
				resultado.put(message, "Mensaje inesperado");
				return resultado.toString();
			}
			
				
			String pwd1=jso.getString("pwd1");
			String pwd2=jso.getString("pwd2");
				
			if(user.getPwd().equals(pwd1)) {
				resultado.put("type", "error");
				resultado.put(message, "La password no puede ser igual a la anterior.");
				return resultado.toString();
			}
			
			if(pwd1.equals(pwd2)) {
					
					user.setPwd(pwd1);
					Manager.get().resetPwd(user);
					resultado.put("resultado", user.toJSON());
					resultado.put("type", "OK");
					
			}else {
					resultado.put("type", error);
					resultado.put(message, "las password no coinciden.");
			}
					
			
		}
		catch (Exception e) {
			resultado.put("type", error);
			resultado.put(message, e.getMessage());
		}
		
		return resultado.toString();
	}
	
	@GetMapping("/getGames")
	public JSONArray getGames(HttpSession session) {
		return Manager.get().getGames();
	}
	
	@PostMapping(value = "/joinToMatchConMap", produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> joinToMatchConMap(HttpSession session, HttpServletResponse response, @RequestBody Map<String, Object> info) {
		User user = (User) session.getAttribute("user");
		if (user==null) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Identíficate antes de jugar");
		} else {
			JSONObject jso = new JSONObject(info);
			String game = jso.getString("game");
			Match match = Manager.get().joinToMatch(user, game);
			HashMap<String, Object> resultado = new HashMap<>();
			resultado.put("type", this.matchMsg);
			resultado.put(this.matchMsg, match);
			return resultado;
		}
	}
	
	@PostMapping(value = "/joinToMatch", produces=MediaType.APPLICATION_JSON_VALUE)
	public String joinToMatch(HttpSession session, HttpServletResponse response, @RequestBody Map<String, Object> info) {
		User user = (User) session.getAttribute("user");
		if (user==null) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Identíficate antes de jugar");
		} else {
			JSONObject jso = new JSONObject(info);
			String game = jso.getString("game");
			Match match = Manager.get().joinToMatch(user, game);
			JSONObject resultado = new JSONObject();
			resultado.put("type", this.matchMsg);
			resultado.put(this.matchMsg, match.toJSON());
			return resultado.toString();
		}
	}
}
