package edu.uclm.esi.games2020.tools;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Tools {
	public Tools() {}
	
	public String md5(String password) throws NoSuchAlgorithmException {
        
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(password.getBytes());
	    byte[] digest = md.digest();
	    return  DatatypeConverter.printHexBinary(digest).toUpperCase();
	         		   
}

}
