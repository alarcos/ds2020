package edu.uclm.esi.games2020.exceptions;

public class MyException extends Exception {

	private static final long serialVersionUID = 1L;

	public MyException(String msg) {
		super(msg);
	}

}