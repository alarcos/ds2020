package edu.uclm.esi.games2020.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

public class TresEnRayaMatch extends Match {
	private DashBoardTER table;
	
	public TresEnRayaMatch() {
		table = new DashBoardTER();
	}
	
	@Override
	protected void setState(User user) {
		IState state = new DominoState();
		user.setState(state);
		state.setUser(user);

	}

	@Override
	public void start() throws IOException {
		this.started = true;
		this.jugadorConElTurno = this.players.get(0);
		super.notifyStart();
	}

	@Override
	protected JSONObject startData(User player) {
		return table.toJSON();
	}

	protected JSONArray getTablero() {
		return table.toJSON().getJSONArray("table");
	}
	@Override
	protected JSONArray getUserData(User user) {
		return new JSONArray();
	}

	@Override
	protected void comprobarGanador() {
		List<Position> matrix = table.getMatrix();
		
		if(haveRow(matrix) || haveCol(matrix) || haveDiagonal(matrix)) {
			ganador = new ArrayList<>();
			ganador.add(jugadorConElTurno);
			
		}
	}
	private boolean haveRow(List<Position> matrix) {
		int aux = matrix.get(0).getValue();		
		if(aux!=0 && aux == matrix.get(1).getValue() && aux == matrix.get(2).getValue())
			return true;
		
		aux = matrix.get(3).getValue();		
		if(aux!=0 && aux == matrix.get(4).getValue() && aux == matrix.get(5).getValue())
				return true;
		
		aux = matrix.get(6).getValue();		
		return (aux!=0 && aux == matrix.get(7).getValue() && aux == matrix.get(8).getValue());
	}
	private boolean haveCol(List<Position> matrix) {
		int aux = matrix.get(0).getValue();		
		if(aux!=0 && aux == matrix.get(3).getValue() && aux == matrix.get(6).getValue())
				return true;
		
		aux = matrix.get(1).getValue();		
		if(aux!=0 && aux == matrix.get(4).getValue() && aux == matrix.get(7).getValue())
				return true;
		aux = matrix.get(2).getValue();		
		return (aux!=0 && aux == matrix.get(5).getValue() && aux == matrix.get(8).getValue());
		
	}
	private boolean haveDiagonal(List<Position> matrix) {
		int aux = matrix.get(4).getValue();		
		
		if(aux!=0 && aux == matrix.get(0).getValue() && aux == matrix.get(8).getValue())
			return true;
			
		return (aux!=0 && aux == matrix.get(2).getValue() && aux == matrix.get(6).getValue());
	}

	@Override
	protected void actualizarTablero(JSONObject jso) {
		if(ganador==null) {
			Position pos = new Position( (JSONObject) jso.get("position") );
			int value = this.players.indexOf(jugadorConElTurno) +1;
			pos.setValue(value);
			this.table.change(pos);	
		}
		
	}

	@Override
	protected boolean legal(WebSocketSession session, JSONObject jso){
		return table.isValid(new Position((JSONObject) jso.get("position")));
	}

	@Override
	protected boolean tablas() {
		//si queda alguna casilla sin valor...
		for(Position p : table.getMatrix())
			if(p.getValue() == 0)
				return false;
		//si no quedan valores por poner...
		return true;
	}

}

