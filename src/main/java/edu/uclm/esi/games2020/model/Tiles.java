package edu.uclm.esi.games2020.model;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Tiles {
	private List<Tile> allTiles;

	public Tiles() {
		this.allTiles = new ArrayList<>();
		
		for(int i=0;i<7;i++)
			for(int j=i ; j<7;j++)
				this.allTiles.add(new Tile(i,j));
	}
	public void suffle() {
		SecureRandom dado = new SecureRandom();
		for (int i=0; i<200; i++) {
			int a = dado.nextInt(28);
			int b = dado.nextInt(28);
			Tile auxiliar = this.allTiles.get(a);
			this.allTiles.set(a, this.allTiles.get(b));
			this.allTiles.set(b, auxiliar);
		}
	}

	public Tile getTile() {
		if(allTiles.isEmpty())
			return null;
		return this.allTiles.remove(0);
	}
}
