package edu.uclm.esi.games2020.model;

public interface IState {
	
	User getUser();
	void setUser(User user);

}
