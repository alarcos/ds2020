package edu.uclm.esi.games2020.model;

import java.util.ArrayList;
import java.util.List;

public class DominoState implements IState {

	private User user;
	private List<Tile> tiles;

	public DominoState() {
		super();
		tiles = new ArrayList<>();
	}
	@Override
	public User getUser() {
		return this.user;
	}
	@Override
	public void setUser(User user) {
		this.user = user;
	}
	public List<Tile> getTiles(){
		return tiles;
	}
	public void addTile(Tile t) {
		tiles.add(t);
	}
	public void removeTile(Tile t) {
		tiles.remove(t);
	}

	

}
