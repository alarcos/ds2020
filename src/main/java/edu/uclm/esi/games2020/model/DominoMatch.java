package edu.uclm.esi.games2020.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

public class DominoMatch extends Match {
	
	private DashBoardDomino table;

	public DominoMatch() {
		super();
		table = new DashBoardDomino();
	}
	@Override
	protected void setState(User user) {
		IState state = new DominoState();
		user.setState(state);
		state.setUser(user);

	}

	@Override
	public void start() throws IOException {
		this.started = true;
		this.jugadorConElTurno =this.players.get(0);
		super.notifyStart();
		

	}

	@Override
	protected JSONObject startData(User player) {
		
		DominoState state = ((DominoState)player.getState());
		JSONObject jso = new JSONObject();
		JSONArray jsa = new JSONArray();
		
		for(int i=0;i<7;i++) {
			Tile tile = this.table.getTile();
			
			// si tengo el 6,6 tengo el turno
			if(tile.equals(new Tile(6,6))) 
				this.jugadorConElTurno = player;
				
			state.addTile(tile);
			jsa.put(tile.toJSON());
			
		}
		
		//envio el nuevo estado
		if(jugadorConElTurno!= null)
			jso.put("turn", jugadorConElTurno.getUserName());
		
		else
			jso.put("turn", "desconocido");
		
		
		jso.put("table", getTablero());
		jso.put("data", jsa);
		return jso;
	}

	@Override
	protected JSONArray getTablero() {
		return table.toJSON().getJSONArray("table");
	}
	@Override
	protected JSONArray getUserData(User user) {
		JSONArray jsa = new JSONArray();
		for(Tile t : ((DominoState)user.getState()).getTiles())
			jsa.put(t.toJSON());
		return jsa;
	}

	@Override
	protected void comprobarGanador() {
		
		if(!jugadorSinFichas() && !hayMovimientos())
				contarPuntos();

	}
	private void contarPuntos() {
		int cnt;
		int min = -1;
		
		for(User u : players) {
			cnt = 0;
			for(Tile t : ((DominoState)u.getState()).getTiles() )
				cnt += t.points();
			
			if(min > cnt) {
				min = cnt;
				ganador = new ArrayList<>();
				ganador.add(u);
			}
			else if (min == cnt) {
				//puede haber empate de ganadores
				ganador.add(u);
			}	
		}
		
	}
	private boolean jugadorSinFichas() {
		//algun jugador no tiene fichas
		for(User u : players) {
			if (((DominoState)u.getState()).getTiles().isEmpty()) {
				ganador = new ArrayList<>();
				ganador.add(u);
				return true;
			}
			
		}
		return false;
	}

	@Override
	protected void actualizarTablero(JSONObject jso) {
		
		boolean robar = (Boolean) jso.get("robar");
		if(robar){
			
			//añado la ficha al usuario
			Tile tile = this.table.getTile();
			if(tile!=null) {
				((DominoState)this.jugadorConElTurno.getState()).addTile(tile);
			
				// para conservar el turno, porgo jugadorconeltrunno igual al anterior jugador
				int pos = this.players.indexOf(jugadorConElTurno);
				if(pos == 0)
					this.jugadorConElTurno =this.players.get(this.players.size()-1);
				else
					this.jugadorConElTurno =this.players.get(pos-1);
			}
			
		}
			else {
			Tile tile = new Tile( (JSONObject) jso.get("tile") );
			
			//le quito la ficha al usuario
			((DominoState)this.jugadorConElTurno.getState()).removeTile(tile);
			//añado la ficha al tablero
			this.table.add(tile);	
		}

	}

	@Override
	protected boolean legal(WebSocketSession session, JSONObject jso){
		boolean robar = (Boolean) jso.get("robar");
		if(robar)
			return true;
		
		Tile tile = new Tile((JSONObject) jso.get("tile"));
		
		//compruebo si la ficha es del jugador actual
		int pos = ((DominoState)this.jugadorConElTurno.getState()).getTiles().indexOf(tile);
		
		return pos!=-1 && table.isValid(tile);
	}
	@Override
	protected boolean tablas() {
		return false;
	}
	private boolean hayMovimientos() {
			return movimientosDer() || movimientosIzq();
	}
	private boolean movimientosIzq() {
		int value;
		
		
		Tile tail = table.getTail();
		if(tail !=null) {
			
			value = tail.getValueA();
			
			//comparo el valor del tablero con las fichas de los usuarios
			//si hay coincidencia no hay tablas
			for(User u : players)
				for(Tile t : ((DominoState)u.getState()).getTiles() )
					if(value == t.getValueA() || value == t.getValueB())
						return true;	
		}
		return false;
	}
	
	private boolean movimientosDer() {
		int value;
		
		Tile head = table.getHead();
		if(head !=null) {

			value = head.getValueB();
			
			//comparo el valor del tablero con las fichas de los usuarios
			//si hay coincidencia no hay tablas
			for(User u : players)
				for(Tile t : ((DominoState)u.getState()).getTiles() )
					if(value == t.getValueA() || value == t.getValueB())
						return true;	
		}
		return false;
	}

}
