package edu.uclm.esi.games2020.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class DashBoardTER {
	
private List<Position> matrix;
	
	public DashBoardTER() {
		this.matrix = new ArrayList<>();
		
		//relleno el tablero
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				matrix.add(new Position(i,j));
	}
	
	public List<Position> getMatrix() {
		return matrix;
	}

	public void change(Position p) {
		
		int i= this.matrix.indexOf(p);
		if(i>-1) {
			this.matrix.get(i);
			if(this.matrix.get(i).getValue()==0)
				//if isValid()
				this.matrix.set(i,p);
			}
		
	}
	public boolean isValid(Position p) {
		
		int i= this.matrix.indexOf(p);
		if(i>-1) {
			this.matrix.get(i);
			if(this.matrix.get(i).getValue()==0)
				return true;
			}
		return false;
	}
	
	public JSONObject toJSON() {
		JSONArray jsa = new JSONArray();
		for(Position p: matrix)
			jsa.put(p.toJSON());
		return new JSONObject().put("table",jsa);
	}

}
