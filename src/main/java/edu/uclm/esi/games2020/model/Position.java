package edu.uclm.esi.games2020.model;

import org.json.JSONObject;

public class Position {
	private int x;
	private int y;
	private int value;
	
	
	public Position(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.value = 0;
	}
	public Position(JSONObject obj) {
		super();
		this.x = (int) obj.get("x");
		this.y = (int) obj.get("y");
		this.value = (int) obj.get("value");
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("x", this.x);
		jso.put("y", this.y);
		jso.put("value", this.value);
		return jso;
	}
	@Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 17 + x;
        hash = hash * 31 + y;

        return hash;
    }
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		
		if(this.getClass() != obj.getClass())
			return false;
		
		Position pos = (Position) obj;
		return (pos.getX()==x && pos.getY()==y);
			
	}

}
