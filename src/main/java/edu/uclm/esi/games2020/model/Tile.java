package edu.uclm.esi.games2020.model;

import org.json.JSONObject;

public class Tile {
	private int valueA;
	private int valueB;

	
	public Tile(int valueA,int valueB) {
		this.valueA= valueA;
		this.valueB= valueB;
	}
	public Tile(JSONObject obj) {
		this.valueA= (int) obj.get("valueA");
		this.valueB= (int) obj.get("valueB");

	}
	
	public int getValueA() {
		return valueA;
	}
	public void setValueA(int valueA) {
		this.valueA = valueA;
	}
	public int getValueB() {
		return valueB;
	}
	public void setValueB(int valueB) {
		this.valueB = valueB;
	}
	

	public void rotate() {
		int aux = this.valueA;
		this.valueA = this.valueB;
		this.valueB = aux;
		
	}

	public int points() {
		return valueA + valueB;
	}
	
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("valueA", this.valueA);
		jso.put("valueB", this.valueB);
		return jso;
	}
	@Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 17 + valueA;
        hash = hash * 31 + valueB;

        return hash;
    }
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		
		if(this.getClass() != obj.getClass())
			return false;

		Tile aux = (Tile) obj;
		return this.valueA == aux.getValueA() && this.valueB == aux.getValueB();

	}
	

}
