package edu.uclm.esi.games2020.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

public abstract class Match {
	protected List<User> players;
	protected User jugadorConElTurno;
	List<User> ganador;
	protected String id;
	protected boolean started;
	private int readyPlayers;
	private Game game;
	
	public Match() {
		this.id = UUID.randomUUID().toString();
		this.players = new ArrayList<>();
	}

	public void addPlayer(User user) {
		this.players.add(user);
		setState(user);
	}
	
	protected abstract void setState(User user); 

	public List<User> getPlayers() {
		return players;
	}
	
	public String getId() {
		return id;
	}

	public abstract void start() throws IOException;
	


	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("idMatch", this.id);
		jso.put("started", this.started);
		JSONArray jsa = new JSONArray();
		for (User user : this.players)
			jsa.put(user.toJSON());
		jso.put("players", jsa);
		jso.put("tablero", this.getTablero());
		return jso;
	}

	public void notifyStart() throws IOException {
		JSONObject jso = this.toJSON();
		jso.put("type", "matchStarted");
		for (User player : this.players) {
			jso.put("startData", startData(player));
			player.send(jso);
		}
	}

	protected abstract JSONObject startData(User player);

	public void playerReady() {
		++readyPlayers;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public boolean ready(){
		return this.readyPlayers==game.requiredPlayers;
	}

	public void ejecutarMovimiento(WebSocketSession session, JSONObject jso) throws IOException{
		if (tablas()) 
			sendError(session,"No hay mas movimientos posibles");
		else if (!tieneTurno(session))
			sendError(session,"No tienes el turno");
		else if (!legal(session, jso)) 
			sendError(session,"Movimiento ilegal");
		
		else{
			actualizarTablero(jso);
			comprobarGanador();
			actualizarClientes();
		}
	}
	
	
	private void sendError(WebSocketSession session,String txt) throws IOException {
		JSONObject obj = new JSONObject();
		obj.put("type","error");
		obj.put("turn",this.jugadorConElTurno.getUserName());
		obj.put("msg",txt);
		session.sendMessage(new TextMessage(obj.toString()));
	}
	private void actualizarClientes() throws IOException {
		JSONObject json = new JSONObject();
		
		if (this.ganador!=null) {
			
			String winner = "";
			for(User u : ganador)
				winner += " "+u.getUserName()+",";
			
			json.put("type", "winner");
			//quito la ultima coma
			json.put("winner", winner.substring(0, winner.length()-1));
			json.put("table", this.getTablero());

		}
		
		else {
			json.put("type", "tablero");
			json.put("table", this.getTablero());
			
			
			int pos = players.indexOf(jugadorConElTurno);
			
			if(pos == players.size()-1)
				this.jugadorConElTurno =this.players.get(0);
			else
				this.jugadorConElTurno =this.players.get(pos+1);
			
			json.put("turn", this.jugadorConElTurno.getUserName());
		}
		
		for (User user : this.players) {
			json.put("data", this.getUserData(user));
			user.send(json);
		}
	}

	protected abstract JSONArray getTablero();
	protected abstract JSONArray getUserData(User user);
	protected abstract void comprobarGanador();
	protected abstract void actualizarTablero(JSONObject jso);
	protected abstract boolean legal(WebSocketSession session, JSONObject jso);
	protected abstract boolean tablas();

	private boolean tieneTurno(WebSocketSession session){
		WebSocketSession sessionTurno = this.jugadorConElTurno.getSession();
		return  (sessionTurno==session);
			
	}
}
