package edu.uclm.esi.games2020.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class DashBoardDomino {
	
	private Tiles tiles;
	private List<Tile> tableroIzq;
	private List<Tile> tableroDer;
	
	public DashBoardDomino() {
		tableroIzq = new ArrayList<>();
		tableroDer = new ArrayList<>();
		tiles = new Tiles();
		tiles.suffle();
	}
	public Tile getTile() {
		return tiles.getTile();
	}
	public Tile getHead() {
		if(tableroDer.isEmpty())
			return null;
		
		return tableroDer.get(tableroDer.size()-1);
	}
	public Tile getTail() {
		if(tableroIzq.isEmpty())
			return null;
		return tableroIzq.get(tableroIzq.size()-1);
	}
	public boolean isValid(Tile tile) {
		int newA = tile.getValueA();
		int newB = tile.getValueB();
		
		//caso inicial
		if(tableroDer.isEmpty() && newA  == 6 && newB  == 6 )
			return true;
		
		//inicio de cola (es necesario que se haya puesto el (6,6) en la cabeza antes)
		if(!tableroDer.isEmpty() && tableroIzq.isEmpty() && (newA == 6 || newB == 6))
			return true;
		
		//con la cabeza iniciada
		if(!tableroDer.isEmpty() ) {
			Tile head = tableroDer.get(tableroDer.size()-1);
			
			// si el valor B de la ficha del tablero coincide con algun valor de la ficha nueva...
			if(head.getValueB() == newA || head.getValueB() == newB)
				return true;
		}
		//con la cola iniciada
		if(!tableroIzq.isEmpty() ) {
			Tile tail = tableroIzq.get(tableroIzq.size()-1);
			
			// si el valor A de la ficha del tablero coincide con algun valor de la ficha nueva...
			if(tail.getValueA() == newA || tail.getValueA() == newB)
				return true;
						
		}
		return false;
	}
	
	public void add(Tile tile) {
		if(!addToDer(tile))
			addToIzq(tile);
			
	}
	private boolean addToIzq(Tile tile) {
		int newA = tile.getValueA();
		int newB = tile.getValueB();
		
		//inicio de cola
				if(tableroIzq.isEmpty()) {
					if(newA  == 6 ) {
						tile.rotate();
						tableroIzq.add(tile);
						return true;
					}
					if (newB  == 6) {
						tableroIzq.add(tile);
						return true;
					}	
				}
				//con la cola iniciada
				else{
					Tile tail = tableroIzq.get(tableroIzq.size()-1);
							
					// si el valor A de la ficha del tablero coincide con algun valor de la ficha nueva...
					if(tail.getValueA() == newA) {
							tile.rotate();
							tableroIzq.add(tile);
							return true;
					}
					if(tail.getValueA() == newB) {
							tableroIzq.add(tile);
							return true;
					}
				}
				return false;
	}
	private boolean addToDer(Tile tile) {
		int newA = tile.getValueA();
		int newB = tile.getValueB();
		
		//inicio de cabeza
		if(tableroDer.isEmpty()) {
				if(newA == 6 && newB == 6) {
					tableroDer.add(tile);
					return true;	
				}
		}
		//con la cabeza iniciada
		else{
				Tile head = tableroDer.get(tableroDer.size()-1);
							
				// si el valor B de la ficha del tablero coincide con algun valor de la ficha nueva...
				if(head.getValueB() == newA) {		
						tableroDer.add(tile);
						return true;
				}
				if(head.getValueB() == newB) {
						tile.rotate();
						tableroDer.add(tile);
						return true;	
				}	
		}
		return false;
	}

	public JSONObject toJSON() {
		JSONArray jsa = new JSONArray();
		// le doy la vuelta a la parte izquierda del tablero para que se vea bien
		List<Tile> reverse = new ArrayList<>(tableroIzq);
		Collections.reverse(reverse);
		
		for(Tile t : reverse)
			jsa.put(t.toJSON());
		
		for(Tile t : tableroDer)
			jsa.put(t.toJSON());
		
		return new JSONObject().put("table",jsa);
		
	}
}
